<?php
return [
    'vanigation_menu' => [
        'title'  => 'Navigation Menu',
        'class'  => \WFN\Navigation\Block\Widget\Menu::class,
        'fields' => [
            [
                'type'  => 'input',
                'name'  => 'identifier',
                'label' => 'Menu Identifier',
            ]
        ],
    ],
];