<?php
return [
    'navigation' => [
        'label' => 'Menu Manager',
        'icon'  => 'icon-menu',
        'route' => 'admin.navigation.menu',
    ],
];