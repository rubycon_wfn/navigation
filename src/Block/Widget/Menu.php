<?php

namespace WFN\Navigation\Block\Widget;

class Menu extends \WFN\CMS\Block\Widget\AbstractWidget
{

    public function render()
    {
        if(empty($this->identifier)) {
            return '';
        }
        return \Menu::render($this->identifier);
    }

}