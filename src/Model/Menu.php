<?php

namespace WFN\Navigation\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\View;

class Menu extends Model
{

    protected $table = 'menu';

    protected $fillable = ['title', 'identifier', 'status'];

    public function items()
    {
        return $this->hasMany(\WFN\Navigation\Model\Menu\Item::class, 'menu_id', 'id')->whereNull('parent_id')->orderBy('position');
    }

    public static function render($identifier)
    {
        $menu = self::where('identifier', $identifier)->where('status', \WFN\CMS\Model\Source\Status::ENABLED)->first();
        if(!$menu) {
            return '';
        }
        $viewName = View::exists('navigation.menu') ? 'navigation.menu' : 'navigation::menu';
        $itemViewName = View::exists('navigation.menu.item') ? 'navigation.menu.item' : 'navigation::menu.item';
        return view($viewName, compact('menu', 'itemViewName'));
    }

}