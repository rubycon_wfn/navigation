<?php

namespace WFN\Navigation\Model\Menu;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $table = 'menu_item';

    protected $fillable = ['menu_id', 'parent_id', 'title', 'link', 'link_target', 'path', 'position', 'identifier', 'css_class', 'cms_block'];

    protected function childrens()
    {
        return $this->hasMany(\WFN\Navigation\Model\Menu\Item::class, 'parent_id', 'id')->orderBy('position');
    }

}