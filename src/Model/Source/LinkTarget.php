<?php

namespace WFN\Navigation\Model\Source;

class LinkTarget extends \WFN\Admin\Model\Source\AbstractSource
{

    const DEFAULT = 1;
    const BLANK   = 2;

    protected function _getOptions()
    {
        return [
            self::DEFAULT => 'Default',
            self::BLANK   => 'New Tab',
        ];
    }
}