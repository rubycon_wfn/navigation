<?php

Route::prefix(env('ADMIN_PATH', 'admin'))->group(function() {
    Route::prefix('navigation')->group(function() {
        Route::get('/', '\WFN\Navigation\Http\Controllers\Admin\MenuController@index')->name('admin.navigation.menu');
        Route::get('/edit/{id}', '\WFN\Navigation\Http\Controllers\Admin\MenuController@edit')->name('admin.navigation.menu.edit');
        Route::get('/new', '\WFN\Navigation\Http\Controllers\Admin\MenuController@new')->name('admin.navigation.menu.new');
        Route::post('/save', '\WFN\Navigation\Http\Controllers\Admin\MenuController@save')->name('admin.navigation.menu.save');
        Route::get('/delete/{id}', '\WFN\Navigation\Http\Controllers\Admin\MenuController@delete')->name('admin.navigation.menu.delete');
    });
});
