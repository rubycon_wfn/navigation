<?php
namespace WFN\Navigation\Providers;

use WFN\Admin\Providers\ServiceProvider as WFNServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\Route;

class ServiceProvider extends WFNServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->booting(function() {
            $loader = AliasLoader::getInstance();
            $loader->alias('Menu', \WFN\Navigation\Model\Menu::class);
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(\Illuminate\Routing\Router $router)
    {
        $basePath = realpath(__DIR__ . '/..');
        Route::middleware('web')->group($basePath . '/routes/web.php');
        
        $this->loadViewsFrom($basePath . '/views', 'navigation');
        $this->publishes([
            $basePath . '/views' => resource_path('views/navigation'),
        ], 'view');

        $this->loadMigrationsFrom($basePath . '/database/migrations');
        $this->mergeConfigFrom($basePath . '/Config/widgets.php', 'widgets');
        $this->mergeConfigFrom($basePath . '/Config/adminNavigation.php', 'adminNavigation');
    }

}
