<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNavigationTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('identifier');
            $table->tinyInteger('status');
            $table->timestamps();
        });

        Schema::create('menu_item', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('menu_id');
            $table->string('title');
            $table->string('link');
            $table->tinyInteger('link_target');
            $table->string('path');
            $table->tinyInteger('position')->nullable(true);
            $table->string('identifier')->nullable(true);
            $table->string('css_class')->nullable(true);

            $table->foreign('menu_id')
                ->references('id')
                ->on('menu')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu');
        Schema::dropIfExists('menu_item');
    }
}
