<li class="nav-item">
    <a href="{{ $item->link }}"
        class="nav-link {{ $item->childrens->count() ? 'dropdown-toggle' : '' }}"
        {{ $item->link_target == \WFN\Navigation\Model\Source\LinkTarget::BLANK ? 'target="_blank"' : '' }}
        @if($item->childrens->count())
        id="dropdownMenu{{ $item->id }}"
        role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
        @endif
    >
        {{ $item->title }}
    </a>
    @if($item->childrens->count())
        <ul aria-labelledby="dropdownMenu{{ $item->id }}" class="dropdown-menu border-0 shadow">
        @foreach($item->childrens as $item)
            @include($itemViewName, ['item' => $item, 'itemViewName' => $itemViewName])
        @endforeach
        </ul>
    @endif
    @if($item->cms_block)
        {!! \WFN\CMS\Block\Widget\Block::getInstance()->setData(['block_id' => $item->cms_block])->render() !!}
    @endif
</li>
