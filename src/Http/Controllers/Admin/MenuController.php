<?php

namespace WFN\Navigation\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use WFN\Navigation\Model\Menu\Item;
use Alert;

class MenuController extends \WFN\Admin\Http\Controllers\Controller
{
    
    protected $_processedItems = [];
    
    public function index(Request $request)
    {
        $grid = new \WFN\Navigation\Block\Admin\Menu\Grid($request);
        return $grid->render();
    }

    public function new()
    {
        $menu = new \Menu();
        return view('navigation::admin.menu.form', compact('menu'));
    }

    public function edit($id = false)
    {
        $menu = \Menu::findOrFail($id);
        return view('navigation::admin.menu.form', compact('menu'));
    }

    public function save(Request $request)
    {
        try {
            $this->menu = new \Menu();
            if($request->input('id')) {
                $this->menu = $this->menu->findOrFail($request->input('id'));
            }

            $data = $request->all();
            $this->validator($data)->validate();
            $this->menu->fill($data)->save();
            $items = $request->input('items');
            $items = json_decode($items, true);

            if(is_array($items)) {
                $this->_processMenuItems($items, $request);
            }

            \WFN\Navigation\Model\Menu\Item::where('menu_id', $this->menu->id)->whereNotIn('menu_item.id', $this->_processedItems)->delete();

            Alert::addSuccess('Navigation menu has been saved');

        } catch (ValidationException $e) {
            foreach($e->errors() as $messages) {
                foreach ($messages as $message) {
                    Alert::addError($message);
                }
            }
        } catch (\Exception $e) {
            Alert::addError($e->getMessage());
        }
        return !$this->menu->id ? redirect()->route('admin.navigation.menu.new') : redirect()->route('admin.navigation.menu.edit', ['id' => $this->menu->id]);
    }

    public function delete($id)
    {
        try {
            $menu = \Menu::findOrFail($id);
            $menu->delete();
            Alert::addSuccess('Navigation menu has been deleted');
        } catch (\Exception $e) {
            Alert::addError($e->getMessage());
        }
        return redirect()->route('admin.navigation.menu');
    }

    protected function validator(array $data)
    {
        $rules = [
            'title'      => 'required|string|max:255',
            'identifier' => 'required|string|max:255' . ($this->menu->identifier != $data['identifier'] ? '|unique:menu' : ''),
        ];
        return Validator::make($data, $rules);
    }

    protected function _processMenuItems($items, $request, $parentItemId = false)
    {
        $itemsData = $request->input('item');
        $position = 0;
        foreach($items as $data) {
            $item = Item::find($data['id']) ?: new Item();
            $itemData = $itemsData[$data['id']];
            if(empty($itemData['title']) || empty($itemData['link_target'])) {
                continue;
            }
            $itemData['menu_id'] = $this->menu->id;
            if($parentItemId) {
                $itemData['parent_id'] = $parentItemId;
            } else {
                $itemData['parent_id'] = null;
            }
            $itemData['position'] = $position++;
            $item->fill($itemData)->save();
            if(!empty($data['children'])) {
                $this->_processMenuItems($data['children'], $request, $item->id);
            }
            $this->_processedItems[] = $item->id;
        }
    }

}